﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrentPurchases
{
    public class InternalSettings
    {
        public bool TestMode { get; set; }
        public string LogFileName { get; set; }
        public string TextEpost { get; set; }
        public bool SkrivLoggFil { get; set; }
        public string tmpBolag { get; set; }
        public string tmpSenaText { get; set; }
        public string tmpEjBekrText { get; set; }

        internal Settings setting { get; set; }

        public InternalSettings(Garp.Application GarpApp)
        {
            setting = Settings.Default;

            TestMode = setting.Testläge;
            SkrivLoggFil = setting.SkrivLoggFil;
            LogFileName = string.Format(@"{0}Current_purchases_{1}.txt", setting.SökvägLoggFil, DateTime.Now.ToString("yyyy-MM-dd HHmm"));
        }
    }
}
