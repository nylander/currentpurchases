﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using System.Net.Mail;

namespace CurrentPurchases
{
    public class Program
    {

        #region Interface mm

        public static Garp.Application GarpApp { get; set; }

        public static Garp.ITable tblIGA { get; set; }
        public static Garp.ITable tblIGR { get; set; }
        public static Garp.ITable tblLA { get; set; }

        public static Garp.ITabField fld_IGA_OrderNr { get; set; }
        public static Garp.ITabField fld_IGA_LevNr { get; set; }
        public static Garp.ITabField fld_IGA_OrderTyp { get; set; }
        public static Garp.ITabField fld_IGA_Reservationskod { get; set; }
        public static Garp.ITabField fld_IGA_VarReferens { get; set; }
        public static Garp.ITabField fld_IGA_ErReferens { get; set; }
        public static Garp.ITabField fld_IGA_OrderDat { get; set; }

        public static Garp.ITabField fld_IGR_OrderNr { get; set; }
        public static Garp.ITabField fld_IGR_RadNr { get; set; }
        public static Garp.ITabField fld_IGR_ArtikelNr { get; set; }
        public static Garp.ITabField fld_IGR_Ben { get; set; }
        public static Garp.ITabField fld_IGR_LagerNr { get; set; }
        public static Garp.ITabField fld_IGR_Levtid { get; set; }
        public static Garp.ITabField fld_IGR_Reservationskod { get; set; }
        public static Garp.ITabField fld_IGR_Radtyp { get; set; }
        public static Garp.ITabField fld_IGR_Antal { get; set; }
        public static Garp.ITabField fld_IGR_LevAntal { get; set; }
        public static Garp.ITabField fld_IGR_LeveransFlagga { get; set; }

        public static Garp.ITabField fld_LA_LevNr { get; set; }
        public static Garp.ITabField fld_LA_Namn { get; set; }

        private static Logininfo login = null;
        private static InternalSettings setting = null;

        #endregion Interface

        static void Main(string[] args)
        {
            try
            {

                login = new Logininfo();
                setting = new InternalSettings(GarpApp);

                Logfil("Starting" + Environment.NewLine);
                GarpLogin();

                #region Hämtar tabeller och fält

                tblIGA = GarpApp.Tables.Item("IGA");
                tblIGR = GarpApp.Tables.Item("IGR");
                tblLA = GarpApp.Tables.Item("LA");

                fld_IGA_OrderNr = tblIGA.Fields.Item("ONR");
                fld_IGA_LevNr = tblIGA.Fields.Item("LNR");
                fld_IGA_OrderTyp = tblIGA.Fields.Item("OTY");
                fld_IGA_Reservationskod = tblIGA.Fields.Item("RES");
                fld_IGA_VarReferens = tblIGA.Fields.Item("VRF");
                fld_IGA_ErReferens = tblIGA.Fields.Item("ERF");
                fld_IGA_OrderDat = tblIGA.Fields.Item("ODT");

                fld_IGR_OrderNr = tblIGR.Fields.Item("ONR");
                fld_IGR_RadNr = tblIGR.Fields.Item("RDC");
                fld_IGR_ArtikelNr = tblIGR.Fields.Item("ANR");
                fld_IGR_Ben = tblIGR.Fields.Item("BEN");
                fld_IGR_LagerNr = tblIGR.Fields.Item("LAG");
                fld_IGR_Levtid = tblIGR.Fields.Item("LDT");
                fld_IGR_Reservationskod = tblIGR.Fields.Item("RES");
                fld_IGR_Radtyp = tblIGR.Fields.Item("RAT");
                fld_IGR_Antal = tblIGR.Fields.Item("ORA");
                fld_IGR_LevAntal = tblIGR.Fields.Item("TLA");
                fld_IGR_LeveransFlagga = tblIGR.Fields.Item("LVF");

                fld_LA_LevNr = tblLA.Fields.Item("LNR");
                fld_LA_Namn = tblLA.Fields.Item("NAM");

                #endregion slut, hämtar tabeller och fält

                bool NotConfirm = EjBekraftat();
                if (NotConfirm)
                {
                    setting.tmpEjBekrText = "These purchases are unconfirmed:" + Environment.NewLine + Environment.NewLine
                        + "Order  Orderdate                    Supplier          Item                 Name Del time     Qty Ref" + Environment.NewLine
                        + setting.tmpEjBekrText + Environment.NewLine;

                    Logfil(setting.tmpEjBekrText, setting.tmpEjBekrText);
                }
                else
                {
                    Logfil("No unconfirmed purchases" + Environment.NewLine);
                }

                bool latePurchase = EjLevererat();
                if (latePurchase)
                {
                    setting.tmpSenaText = "These deliveries are delayed:" + Environment.NewLine + Environment.NewLine
                        + "Order  Orderdate                    Supplier          Item                 Name Del time     Qty Ref" + Environment.NewLine
                        + setting.tmpSenaText;

                    Logfil(setting.tmpSenaText, setting.tmpSenaText);
                }
                else
                {
                    Logfil("No late deliveries" + Environment.NewLine);
                }

                EndGarp();

                SendEpost();

                Logfil("Done");

                if (setting.TestMode == true)
                {
                    Console.ReadKey();
                }

            }
            catch (Exception e)
            {
                Logfil(e + Environment.NewLine + e.Message + Environment.NewLine);
                if (setting.TestMode == true)
                {
                    Console.ReadLine();
                }

            }

        }

        private static bool EjLevererat()
        {

            string TestDag = DateTime.Now.AddDays(-2).ToString("yyMMdd");

            setting.tmpSenaText = string.Empty;

            Garp.ITabFilter findOlevererat = tblIGR.Filter;
            findOlevererat.AddField("LVF");
            findOlevererat.AddConst("5");
            findOlevererat.Expression = "1<2"; // Ej levererade inköp
            findOlevererat.Active = true;
            tblIGR.First();

            while (tblIGR.Eof != true)
            {
                tblIGA.Find(fld_IGR_OrderNr.Value);

                // Ej förfrågan, ej planerat avrop, ej bekräftade avropsrader, levransdatumet pasderat 2 dagar
                if (
                    fld_IGA_OrderTyp.Value != "0" // Förfrågan
                    && fld_IGA_Reservationskod.Value != "P" // Planerat avrop
                    && fld_IGR_Reservationskod.Value != "B" // Bekräftat avrop
                    && fld_IGR_Reservationskod.Value != "0" // Bekräftat avrop
                    && string.Compare(TestDag, fld_IGR_Levtid.Value) > 0
                    )
                {
                    string LevNamn = null;

                    if (tblLA.Find(fld_IGA_LevNr.Value))
                    { LevNamn = fld_LA_Namn.Value; }
                    else
                    { LevNamn = "Missing"; }

                    string Levtid = fld_IGR_Levtid.Value;
                    if (Levtid == null) { Levtid = "Blank"; }

                    string ArtNr = fld_IGR_ArtikelNr.Value;
                    if (ArtNr == null) { ArtNr = "Missing"; }

                    string Ben = fld_IGR_Ben.Value;
                    if (Ben == null) { Ben = "Missing"; }

                    string Ref = fld_IGA_VarReferens.Value;
                    if (Ref == null) { Ref = "  "; }

                    double Antal = double.Parse(fld_IGR_Antal.Value, CultureInfo.InvariantCulture) - double.Parse(fld_IGR_LevAntal.Value, CultureInfo.InvariantCulture);
                    string RestAntal = Antal.ToString();

                    setting.tmpSenaText +=
                        Verktyg.fillBlankRight(fld_IGR_OrderNr.Value, 6)
                        + Verktyg.fillBlankLeft(fld_IGA_OrderDat.Value, 7)
                        + Verktyg.fillBlankLeft(LevNamn, 31)
                        + Verktyg.fillBlankLeft(ArtNr, 14)
                        + Verktyg.fillBlankLeft(Ben, 21)
                        + Verktyg.fillBlankLeft(Levtid, 7)
                        + Verktyg.fillBlankLeft(RestAntal, 10)
                        + Verktyg.fillBlankLeft(Ref, 3)
                        + Environment.NewLine
                        ;

                }

                tblIGR.Next();
            }
            if (setting.tmpSenaText != string.Empty) { return true; }
            else { return false; }

        }

        private static bool EjBekraftat()
        {
            string TestDag = DateTime.Now.AddDays(-2).ToString("yyMMdd");

            setting.tmpEjBekrText = string.Empty;

            Garp.ITabFilter findEjBekr = tblIGR.Filter;
            findEjBekr.AddField("LVF"); // 1. Levflagga
            findEjBekr.AddField("RES"); // 2. Reservationskod
            findEjBekr.AddConst("5");   // 3.
            findEjBekr.AddConst(" ");   // 4. Blankt
            findEjBekr.AddConst("P");   // 5. 
            findEjBekr.Expression = "1<3&(2m4/2=5)"; // Ej levererade, ej bekräftade inköp
            findEjBekr.Active = true;
            tblIGR.First();

            while (tblIGR.Eof != true)
            {
                tblIGA.Find(fld_IGR_OrderNr.Value);

                // Ej förfrågan
                if (fld_IGA_OrderTyp.Value != "0" && string.Compare(TestDag, fld_IGA_OrderDat.Value) > 0)
                {
                    string LevNamn = null;

                    if (tblLA.Find(fld_IGA_LevNr.Value))
                    { LevNamn = fld_LA_Namn.Value; }
                    else
                    { LevNamn = "Missing"; }

                    string Levtid = fld_IGR_Levtid.Value;
                    if (Levtid == null) { Levtid = "Blank"; }

                    string ArtNr = fld_IGR_ArtikelNr.Value;
                    if (ArtNr == null) { ArtNr = "Missing"; }

                    string Ben = fld_IGR_Ben.Value;
                    if (Ben == null) { Ben = "Missing"; }

                    string Ref = fld_IGA_VarReferens.Value;
                    if (Ref == null) { Ref = "  "; }

                    double Antal = double.Parse(fld_IGR_Antal.Value, CultureInfo.InvariantCulture) - double.Parse(fld_IGR_LevAntal.Value, CultureInfo.InvariantCulture);
                    string RestAntal = Antal.ToString();

                    string Reskod = fld_IGR_Reservationskod.Value;
                    if (Reskod == "P")
                    {
                        Reskod = "Blanket";
                    }

                    setting.tmpEjBekrText +=
                        Verktyg.fillBlankRight(fld_IGR_OrderNr.Value, 6)
                        + Verktyg.fillBlankLeft(fld_IGA_OrderDat.Value, 7)
                        + Verktyg.fillBlankLeft(LevNamn, 31)
                        + Verktyg.fillBlankLeft(ArtNr, 14)
                        + Verktyg.fillBlankLeft(Ben, 21)
                        + Verktyg.fillBlankLeft(Levtid, 7)
                        + Verktyg.fillBlankLeft(RestAntal, 10)
                        + Verktyg.fillBlankLeft(Ref, 3)
                        + " " + Reskod
                        + Environment.NewLine
                        ;

                }

                tblIGR.Next();
            }
            if (setting.tmpEjBekrText != string.Empty) { return true; }
            else { return false; }
        }

        private static void Logfil(string text, string epostText = null)
        {
            Console.Write(text);
            setting.TextEpost += epostText;
            if (setting.SkrivLoggFil == true)
            {
                File.AppendAllText(setting.LogFileName, text);
            }
        }

        private static void SendEpost()
        {
            if (setting.tmpSenaText != string.Empty || setting.tmpEjBekrText != string.Empty)
            {
                try
                {

                    Settings MailSetting = Settings.Default;
                    string mailTo = MailSetting.MailTo;
                    string from = MailSetting.MailFrom;
                    string CC = MailSetting.MailToKopia;
                    string BCC = MailSetting.MailToHemligKopia;
                    string subjekt = MailSetting.MailRubrik;

                    if (setting.tmpEjBekrText != string.Empty)
                    {
                        subjekt += ", not confirmed";
                    }

                    if (setting.tmpSenaText != string.Empty)
                    {
                        subjekt += ", delayed";
                    }


                    string bodytext = "Hello.<br /><br />" + setting.TextEpost.Replace(" ", "&nbsp;").Replace(Environment.NewLine, "<br />") + "<br />";
                    string body = "<font style=font-family:Courier New>" + bodytext + "</font>";

                    int port = MailSetting.MailPort;
                    string host = MailSetting.MailHost;
                    bool enableSsl = MailSetting.MailSsl;
                    string user = MailSetting.MailAnvändare;
                    string password = MailSetting.MailLösen;

                    Logfil("Sends Email to " + MailSetting.MailTo + Environment.NewLine);

                    SmtpClient client = new SmtpClient();
                    client.Port = port;
                    client.Host = host;
                    client.EnableSsl = enableSsl;
                    client.Timeout = 10000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    //SmtpServer.UseDefaultCredentials = true;
                    if (user != "")
                    {
                        client.Credentials = new System.Net.NetworkCredential(user, password);
                    }

                    MailMessage mail = new MailMessage(from, mailTo, subjekt, body);
                    if (CC != "" && CC != null) { mail.CC.Add(CC); }
                    if (BCC != "" && BCC != null) { mail.Bcc.Add(BCC); }
                    //mail.BodyEncoding = UTF8Encoding.UTF8;
                    mail.IsBodyHtml = true;
                    mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                    client.Send(mail);

                    Logfil("Email sent" + Environment.NewLine);

                }
                catch (Exception e)
                {
                    Logfil("There was an error sending E-mail" + Environment.NewLine + "Message: " + e.Message + Environment.NewLine + e);
                }
            }
        }

        private static void GarpLogin()
        {
            try
            {
                GarpApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GarpApp = new Garp.Application();

                //Loggar in i Garp och sätter bolag, användare och lösen
                Logfil("Log in to Garp. ");
                if (Settings.Default.AnvändKonfiguration == true)
                {
                    GarpApp.ChangeConfig(login.GarpConfig);
                }
                GarpApp.Login(login.Usr, login.Password);
                GarpApp.SetBolag(login.Bolag);

                if (GarpApp.User == null)
                {
                    GarpApp = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    Logfil("Unable to log in to Garp!" + Environment.NewLine);
                    if (setting.TestMode == true)
                    {
                        Console.ReadKey();
                    }

                    Environment.Exit(0);
                }

                Logfil("Logged in as " + GarpApp.User + ", terminal " + GarpApp.Terminal + " and company " + GarpApp.Bolag + Environment.NewLine);
            }
            catch (Exception e)
            {
                GarpApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Logfil("Unable to log in to Garp!" + Environment.NewLine + e + Environment.NewLine + e.Message);
                if (setting.TestMode == true)
                {
                    Console.ReadKey();
                }

                Environment.Exit(0);
            }
        }

        private static void EndGarp()
        {
            //Stäng Garp
            GarpApp = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            Logfil("Closing Garp" + Environment.NewLine);
        }

    }
}
