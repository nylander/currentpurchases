﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrentPurchases
{
    public class Logininfo
    {
        public string Bolag { get; set; }
        public string Usr { get; set; }
        public string Password { get; set; }
        public string GarpConfig { get; set; }

        internal Settings login { get; set; }

        public Logininfo()
        {
            login = Settings.Default;

            Bolag = login.Bolag;
            Usr = login.Användare;
            Password = login.Lösen;
            GarpConfig = login.Konfiguration;
        }
    }
}
